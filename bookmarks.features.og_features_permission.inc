<?php
/**
 * @file
 * bookmarks.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function bookmarks_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:chanel:add user'
  $permissions['node:chanel:add user'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:administer group'
  $permissions['node:chanel:administer group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:approve and deny subscription'
  $permissions['node:chanel:approve and deny subscription'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:create bm_link content'
  $permissions['node:chanel:create bm_link content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:chanel:delete any bm_link content'
  $permissions['node:chanel:delete any bm_link content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:chanel:delete own bm_link content'
  $permissions['node:chanel:delete own bm_link content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:chanel:manage members'
  $permissions['node:chanel:manage members'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:manage permissions'
  $permissions['node:chanel:manage permissions'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:manage roles'
  $permissions['node:chanel:manage roles'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:subscribe'
  $permissions['node:chanel:subscribe'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:chanel:subscribe without approval'
  $permissions['node:chanel:subscribe without approval'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:chanel:unsubscribe'
  $permissions['node:chanel:unsubscribe'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:chanel:update any bm_link content'
  $permissions['node:chanel:update any bm_link content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:chanel:update group'
  $permissions['node:chanel:update group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:chanel:update own bm_link content'
  $permissions['node:chanel:update own bm_link content'] = array(
    'roles' => array(),
  );

  return $permissions;
}
